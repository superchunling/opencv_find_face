package com.example.opencv_find_face;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener {

    private   JavaCameraView cameraView;
    private FaceDetection mFaceDetection;
    private File mCascadeFile;
    int CameraId=Camera.CameraInfo.CAMERA_FACING_FRONT;
    String[] perms = {Manifest.permission.CAMERA,
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, "该软件需要摄像头权限", 0, perms);
        }
        mFaceDetection = new FaceDetection();
        cameraView=findViewById(R.id.sample_text);
        cameraView.setCvCameraViewListener(this);
        cameraView.setCameraIndex(CameraId);

        copyCascadeFile();
        mFaceDetection.loadCascade(mCascadeFile.getAbsolutePath());
    }


    @Override
    protected void onResume() {
        super.onResume();
        cameraView.enableView();
        Toast.makeText(this,"人像信息存储在内存卡Ling目录下",Toast.LENGTH_LONG);
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraView.disableView();
    }
    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }
    // 获取相机的数据 -> Mat -> SurfaceView 上
    @Override
    public Mat onCameraFrame(Mat inputFrame) {
        // 在这里写业务逻辑
        //画面旋转
/*        if (this.getResources().getConfiguration().orientation
                == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                && cameraView.getCameraIndex() == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            Core.rotate(inputFrame, inputFrame, Core.ROTATE_90_COUNTERCLOCKWISE);
        }else if (this.getResources().getConfiguration().orientation
                == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                && cameraView.getCameraIndex() == Camera.CameraInfo.CAMERA_FACING_BACK) {
            Core.rotate(inputFrame, inputFrame, Core.ROTATE_90_CLOCKWISE);
        }*/
        mFaceDetection.faceDetection(inputFrame,CameraId);
        return inputFrame;
    }
    public void bt(View v){
        if (CameraId==Camera.CameraInfo.CAMERA_FACING_BACK){
            CameraId=Camera.CameraInfo.CAMERA_FACING_FRONT;
        }else {
            CameraId=Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        cameraView.setCameraIndex(CameraId);
        cameraView.disableView();
        cameraView.enableView();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
    private void copyCascadeFile() {

        File path = new File(Environment.getExternalStorageDirectory()+ "/Ling");
        if (!path.exists()) {
            // 创建一个目录,用于存放采集到的人脸照片
            path.mkdirs();
        }
        try {
            // load cascade file from application resources
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            if (mCascadeFile.exists()) return;//文件存在跳过
            FileOutputStream os = new FileOutputStream(mCascadeFile);
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
