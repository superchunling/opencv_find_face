package com.example.opencv_find_face;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class Utils {
    public static void copyAssets(Context context, String path){
        File model=new File(path);
        File file=new File(Environment.getExternalStorageDirectory(),model.getName());

        if (file.exists()){
            //说明文件存在
            file.delete();
        }else {

        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
           // InputStream inputStream = context.getAssets().open(path);
            InputStream inputStream = context.getResources().openRawResource(R.raw.lbpcascade_frontalface);
            byte[] b = new byte[2048];
            int len = 0;
            //读取文件内容:
            while ((len = inputStream.read(b)) != -1) {
                fileOutputStream.write(b, 0, len);
            }
            //关闭输入流
            inputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
