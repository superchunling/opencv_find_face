package com.example.opencv_find_face;

import org.opencv.core.Mat;

class FaceDetection {
    static {
        System.loadLibrary("native-lib");
    }

    /**
     * 检测人脸并保存人脸信息
     *
     * @param mat 当前帧
     */
    public void faceDetection(Mat mat,int CameraId) {
        faceDetection(mat.nativeObj,CameraId);
    }

    /**
     * 加载人脸识别的分类器文件
     *
     * @param filePath
     */
    public native void loadCascade(String filePath);

    private native void faceDetection(long nativeObj,int CameraId);
}
