# opencv_find_face

#### 介绍
使用Opencv实现以下功能
1.人脸识别（支持多人同时识别）
2.采集到人像照片并存入LIng将文件夹下（二值化处理后的灰度图）



#### 安装教程

1.  apk下载地址：[下载](https://gitee.com/jiangchunling/opencv_find_face/blob/master/app/release/app-release.apk)
2.  app需要摄像头权限
3.  所有被apk识别的人脸图形，存放手机内存卡的Ling文件夹下

#### 使用说明

1.  底层使用的Opencv动态库做图像识别，因此必须引入动态链接库和头文件
2.  进行人脸识别时，必须先加载OpenCV训练库(本app训练库存放在项目raw文件夹下)
3.  在native层实现楼NV21数据转Mat。（如果是其他格式图像数据请自行转换）

